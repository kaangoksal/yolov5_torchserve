set -eu
torchserve --start --model-store . --models ${MODEL_LIST} --foreground --no-config-snapshots --ts-config ts.config