
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $SCRIPT_DIR
cd ..

array=( "$@" )
for i in "${array[@]}"
do
	python yolov5/export.py --weights model_weights/${i}.pt  --img 640 --batch 1
  mv model_weights/${i}.torchscript.pt work_dir/${i}.torchscript.pt
done

