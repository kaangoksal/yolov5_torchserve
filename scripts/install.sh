SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $SCRIPT_DIR
cd ..

virtualenv -p /usr/bin/python3 venv
source venv/bin/activate


pip3 install -r requirements.txt
pip3 install -r yolov5/requirements.txt
