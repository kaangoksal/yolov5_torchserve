SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $SCRIPT_DIR
cd ..

MODEL_LIST="yolov5s=yolov5s.mar"
set -eu
torchserve --start --model-store work_dir --models ${MODEL_LIST}  --foreground --no-config-snapshots --ts-config ts.config

