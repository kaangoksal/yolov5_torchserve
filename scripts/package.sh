

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $SCRIPT_DIR
cd ..

array=( "$@" )
for i in "${array[@]}"
do
  echo "Packaging ${i}"
	torch-model-archiver --model-name ${i} \
  --version 0.1 --serialized-file work_dir/${i}.torchscript.pt \
  --handler torchserve_handler_yolov5.py \
  --extra-files index_to_name.json,torchserve_handler_yolov5.py
  mv ${i}.mar work_dir/${i}.mar
done



