FROM ubuntu:20.04

COPY work_dir/yolov5s.mar ./home/yolov5s.mar
COPY work_dir/yolov5l.mar /home/yolov5l.mar
COPY work_dir/yolov5m.mar /home/yolov5m.mar
COPY work_dir/yolov5x.mar /home/yolov5x.mar
COPY entry_point.sh /home/entry_point.sh
COPY requirements.txt ./home/requirements.txt
COPY ts.config ./home/ts.config
WORKDIR /home

ENV TZ=Europe/London

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install python3-pip openjdk-11-jre-headless
RUN pip install -r /home/requirements.txt

CMD ["bash" , "./entry_point.sh"]
#CMD ["torchserve", "--start", "--model-store", ".","--models", "yolov5s=yolov5s.mar", "yolov5l=yolov5l.mar", "yolov5m=yolov5m.mar", "yolov5x=yolov5x.mar","--foreground", "--no-config-snapshots", "--ts-config", "ts.config"]